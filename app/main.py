from flask import Flask, request, render_template, session, redirect
from datetime import timedelta
import json, math
import os
import sys
sys.path.append(os.path.join(os.getcwd(), 'app', 'modules'))
from functions import show_cookies
from mysql_connect import get_cursor

app = Flask(__name__, '/')

app.config['SECRET_KEY'] = os.urandom(24)
app.config['PERMANENT_SESSION_LIFETIME'] = timedelta(minutes=30)


@app.route('/')
def index():
    return render_template('base.html')


@app.route('/save-headers')
def save_headers():
    dict1 = {}
    for (a, b) in request.headers:
        print(a, b)
        dict1[a] = b

    file1 = open('headers.json', 'w')
    file1.write(json.dumps(dict1))
    return dict1


@app.route('/tpl1')
def tpl1():
    return render_template('hello.html', name='David')


@app.route('/tpl2')
def tpl2():
    dict1 = {
        'name': 'Bill',
        'age': 30
    }
    # return render_template('hello.html', name='David', age=20)
    return render_template('hello2.html', **dict1)


@app.route('/my-path')
def my_path():
    # os.getcwd() 取得專案的路徑
    return os.path.join(os.getcwd(), 'app', 'modules')


@app.route('/try-qs')
def query_string():
    # http://localhost:5000/try-qs?a[]=1&b=2&a[]=3
    output = {
        'args': request.args,
        'a[]': request.args.getlist('a[]')
    }
    return output


@app.route('/try-post', methods=['POST'])
def try_post():
    output = {
        'form': request.form,
        'a[]': request.form.getlist('a[]')
    }
    return output


@app.route('/try-post', methods=['GET'])
def try_post_get():
    return request.method


@app.route('/try-post2', methods=['POST'])
def try_post2():
    output = {
        'content_type': request.content_type,
        'data': request.data.decode('utf-8'),
        'json': request.get_json()
    }
    return output


@app.route('/params/')
@app.route('/params/<action>/')
@app.route('/params/<action>/<int:id>')
def params_test(action='none', id=0):
    return {
        'action': action,
        'id': id
    }


@app.route('/try-session')
def try_session():
    if not session.get('what'):
        session['what'] = 1
    else:
        session['what'] += 1
    return str(session.get('what'))


@app.route('/try-mysql')
def try_mysql():
    (cursor, cnx) = get_cursor()
    sql = "SELECT * FROM address_book"
    cursor.execute(sql)
    t_data = cursor.fetchall()
    cnx.commit()
    return render_template('mysql_test.html', t_data=t_data)


@app.route('/address-book-mysql/edit/<int:sid>', methods=['GET'])
def address_book_mysql_edit_get(sid):
    (cursor, cnx) = get_cursor()
    sql = "SELECT * FROM address_book WHERE sid=%(sid)s"
    cursor.execute(sql, {'sid': sid})
    row = cursor.fetchone()
    cnx.commit()
    return render_template('address-book-mysql/edit.html', row=row)


@app.route('/address-book-mysql/edit/', methods=['POST'])
def address_book_mysql_edit_post():
    # TODO: 欄位檢查
    (cursor, cnx) = get_cursor()
    output = {
        'success': False,
        'form_data': request.form
    }
    if request.form['email'] == '':
        output['error'] = '沒有 email 資料'
        return output

    sql = ("UPDATE `address_book` SET "
           "`name`=%(name)s,`email`=%(email)s,`mobile`=%(mobile)s,"
           "`birthday`=%(birthday)s,`address`=%(address)s  WHERE sid=%(sid)s")
    cursor.execute(sql, request.form)
    output['row_count'] = cursor.rowcount
    if cursor.rowcount:
        output['success'] = True
    cnx.commit()

    return output


@app.route('/address-book-mysql/del/<int:sid>')
def address_book_mysql_del(sid):
    (cursor, cnx) = get_cursor()
    sql = "DELETE FROM address_book WHERE sid=%(sid)s"
    cursor.execute(sql, {'sid': sid})
    row_count = cursor.rowcount
    cnx.commit()
    referer = request.headers.get('referer')
    print({
        'sid': sid,
        'row_count': row_count,
        'referer': referer
    })
    if not referer:
        return redirect('/address-book-mysql/list/1', code=302)
    else:
        return redirect(referer, code=302)



@app.route('/address-book-mysql/list/')
@app.route('/address-book-mysql/list/<int:page>')
def address_book_mysql_list(page=1):
    per_page = 5
    output = {
        'page': page,
        'per_page': per_page,
        'total_rows': 0,
        'total_pages': 0,
        'rows': []
    }
    (cursor, cnx) = get_cursor()
    sql = "SELECT COUNT(1) num FROM address_book"
    cursor.execute(sql)
    output['total_rows'] = total_rows = cursor.fetchone()['num']
    if total_rows == 0:
        return render_template('address-book-mysql/list.html', **output)

    output['total_pages'] = math.ceil(total_rows/per_page)
    if page < 1:
        return redirect('/address-book-mysql/list/1')
    if page > output['total_pages']:
        return redirect("/address-book-mysql/list/{}".format(output['total_pages']))

    sql = "SELECT * FROM address_book ORDER BY sid DESC LIMIT {}, {}".format((page-1)*per_page, per_page)
    # print(sql)
    cursor.execute(sql)
    output['rows'] = cursor.fetchall()
    cnx.commit()
    return render_template('address-book-mysql/list.html', **output)

# 官方 cursor 類型說明
# https://dev.mysql.com/doc/connector-python/en/connector-python-api-mysqlcursor.html


@app.route('/address-book-mysql/add', methods=['GET', 'POST'])
def address_book_mysql_add():
    if request.method == 'GET':
        return render_template('address-book-mysql/add.html')
    else:
        # TODO: 欄位檢查
        (cursor, cnx) = get_cursor()
        output = {
            'success': False,
            'form_data': request.form
        }
        # print('email:', request.form['email'])
        if request.form['email']=='':
            output['error'] = '沒有 email 資料'
            return output

        sql = ("INSERT INTO `address_book`("
               "`name`, `email`, `mobile`, `birthday`, `address`, `created_at`"
               ") VALUES (%(name)s, %(email)s, %(mobile)s, %(birthday)s, %(address)s, NOW())")
        cursor.execute(sql, request.form)
        output['last_row_id'] = cursor.lastrowid
        output['row_count'] = cursor.rowcount
        if cursor.rowcount:
            output['success'] = True
        cnx.commit()

        return output


app.add_url_rule('/show-cookies', None, show_cookies)
