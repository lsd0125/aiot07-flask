import mysql.connector

connect_data = {
    'host': 'localhost',
    'user': 'root',
    'passwd': '',
    'database': 'test'
}

cnx = None


def get_connection():
    global cnx
    if not cnx:
        cnx = mysql.connector.connect(** connect_data)
    return cnx


def get_cursor():
    cursor = get_connection().cursor(dictionary=True)
    return cursor, get_connection()
